#include <stdio.h>
#include <ncurses.h>
#include <menu.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>

#include "login.h"
#include "misc.h"
#include "menu_templates.h"

extern char **environ;

int welcome_screen();

int
main(int argc, char **argv){
  initscr(); cbreak(); noecho(); keypad(stdscr, TRUE); curs_set(0);
  if (!has_colors())
    return endwin(), printf("Your terminal doesn't support colour\n"), 1;
  start_color();
  init_pair(1, COLOR_BLACK, COLOR_GREEN);
  char *passwd_path, *data_dir = NULL, *games_path, *name = NULL, *tmp;
  FILE *passwd_file;
  size_t num_items;
  int selection;
  pid_t child_pid;

  // figure out where the passwd file is
  // passwd_path strdups "read only" strings to safely free them later (instead
  // of complex logic checking whether it needs to be freed or not)
  if (argc > 1)
    passwd_path = strdup(argv[argc-1]);
  else if (!(tmp = getenv("GAME_BOX_PASSWD"))) {
    if (errno == ENOMEM)
      return perror("getenv failed"), endwin(), 1;
    else {
      if (!(tmp = getenv("XDG_DATA_HOME"))) {
        if (errno == ENOMEM)
          return perror("getenv failed"), endwin(), 1;
        else {
          if (!(tmp = getenv("HOME"))) // $HOME guranteed by POSIX afaik
            return perror("getenv failed"), endwin(), 1;
          else {
            data_dir =
              malloc((strlen(tmp)+strlen("/.local/share/game-box")+1)
                  *sizeof(char));
            sprintf(data_dir, "%s/.local/share/game-box", tmp);
            passwd_path =
              malloc((strlen(tmp)+strlen("/.local/share/game-box/passwd")+1)
                  *sizeof(char));
            sprintf(passwd_path, "%s/.local/share/game-box/passwd", tmp);
          }
        }
      }
      else {
        data_dir = malloc((strlen(tmp)+strlen("/game-box")+1)*sizeof(char));
        sprintf(data_dir, "%s/game-box", tmp);
        passwd_path = malloc((strlen(tmp)+strlen("/game-box/passwd")+1)
            *sizeof(char));
        sprintf(passwd_path, "%s/game-box/passwd", tmp);
      }
    }
  }
  else
    passwd_path = strdup(tmp); 

  // figure out game directory; defaults to XDG_DATA_HOME/game-box/games
  if (!(tmp = getenv("GAME_BOX_PATH"))) {
    if (errno == ENOMEM)
      return perror("getenv failed"), endwin(), 1;
    else {
      games_path = malloc((strlen(data_dir)+strlen("/games")+1)*sizeof(char));
      sprintf(games_path, "%s/games", data_dir);
    }
  }
  else
    games_path = strdup(tmp);

  if (welcome_screen() < 0)
    return endwin(), 0;
  erase(), refresh();

  if (data_dir)
    mkdir(data_dir, S_IRWXU|S_IRGRP|S_IXGRP|S_IXOTH);
  if (!(passwd_file = fopen(passwd_path, "a+")))
      return endwin(), fprintf(stderr, "cannot open password file\n"), 1;
  free(passwd_path), free(data_dir);

  while ((selection = list_menu((char*[]){"login", "register"}, 2)) >= 0) {
    if (selection == 1)
      register_form(passwd_file);
    else if ((name = login_form(passwd_file)))
      break;
    erase(), refresh();
  }
  fclose(passwd_file);
  if (selection < 0)
    return endwin(), 0;
  erase(), refresh();
  free(name); // don't need it yet; but for leaderboards

  char **list = NULL;
  num_items = exec_list(games_path, &list);
  if ((selection = list_menu(list, num_items)) < 0)
    return endwin(), 0;

  def_prog_mode(), endwin();
  if (!(child_pid = fork())) {
    char *selection_path =
      malloc((strlen(games_path)+strlen(list[selection])+2)*sizeof(char));
    sprintf(selection_path, "%s/%s", games_path, list[selection]);
    free(games_path);
    if (execve(selection_path, (char*[]){list[selection], NULL}, environ) < 0) {
      fprintf(stderr, "%s", strerror(errno));
      free_exec_list(list);
      return 0;
    }
  }
  else if (child_pid < 0) {
    endwin();
    fprintf(stderr, "Fork failed!");
  }
  wait(&child_pid);
  free_exec_list(list);
  free(games_path);
  reset_prog_mode(), erase();
  mvprintw(0, 0, "Done!"), refresh();
  getch();

  endwin();
  return 0;
}

/*
 * displays a welcome screen. Separated into a function as the welcome screen
 * could change (a lot). Returns 0 if passed; -1 if F1 pressed.
 */
int
welcome_screen(){
  int const msg_width = 45, msg_height = 5,
      cols_cntr = COLS / 2 - msg_width / 2;
  int lines_cntr = LINES / 2 - msg_height / 2, ch;

  mvprintw(lines_cntr++, cols_cntr, "  ____                      ____");
  mvprintw(lines_cntr++, cols_cntr, " / ___| __ _ _ __ ___   ___| __ )  _____  __");
  mvprintw(lines_cntr++, cols_cntr, "| |  _ / _` | '_ ` _ \\ / _ \\  _ \\ / _ \\ \\/ /");
  mvprintw(lines_cntr++, cols_cntr, "| |_| | (_| | | | | | |  __/ |_) | (_) >  < ");
  mvprintw(lines_cntr, cols_cntr, " \\____|\\__,_|_| |_| |_|\\___|____/ \\___/_/\\_\\");

  attron(A_BLINK);
  mvprintw(LINES/2+msg_height, COLS/2-15, "-- Press Enter to continue --");
  attroff(A_BLINK);
  refresh();
  while ((ch = getch()) != '\n')
    if (ch == KEY_F(1))
      return endwin(), -1;
  return 0;
}
