#ifndef MISC_H
#define MISC_H

int longest_str(char *const *const str_list, const size_t len);
char *str_no_tws(char const *const str);
char *arbstr(int len_step, FILE *stream);
size_t exec_list(char const *const path, char ***list);
void free_exec_list(char **list);
int getnum(char const *const num_str, long *const num);
#endif
