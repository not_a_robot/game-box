/* Displays a leaderboard from given files. */
/*
 * File format is as follows:
 * leaderboard title; '\n'; score field name; '\n'; max score width; '\n';
 * other data fields; two '\n's
 *
 * name; '\n'; score; '\n'; other data; any number of '\n's; repeat for all
 * names
 *
 * other data fields specified like so: field name; '\n'; max field width;
 * '\n'; next field
 *
 * other data fields are displayed in order given
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>

#include "misc.h"

typedef struct Field {
  long width;
  char *name;
} Field;

int fill_field(FILE *file, Field *const field);
void usage(void);

int
main(int argc, char **argv){
  int i, j, num_spaces, num_fields;
  Field score, *other_fields = NULL;
  char *title, ch;
  FILE *file;
  void *alloc_tmp;

  if (argc != 2)
    return usage(), 1;

  if (!(file = fopen(argv[1], "r")))
    return perror("fopen"), 1;

  if (!(title = arbstr(16, file)))
    return fclose(file), perror("arbstr"), 1;

  if (fill_field(file, &score) < 0) {
    if (errno == EINVAL)
      fprintf(stderr, "encountered non-digit in score width\n");
    else
      perror("fill_field");
    return fclose(file), 1;
  }

  // get all other arbitrary fields
  for (num_fields = 0; (ch = getc(file)) != '\n' && ch != EOF; num_fields++) {
    if (fseek(file, -1, SEEK_CUR) < 0)
      return fclose(file), perror("fseek"), 1;
    if (!(alloc_tmp = realloc(other_fields, (num_fields+1)*sizeof(Field))))
      return free(other_fields), fclose(file), perror("realloc"), 1;
    other_fields = alloc_tmp;

    if (fill_field(file, &other_fields[num_fields]) < 0) {
      if (errno == EINVAL)
        fprintf(stderr, "encountered non-digit in field width\n");
      else
        perror("fill_field");
      return free(other_fields), fclose(file), 1;
    }
  }
  if (fseek(file, -1, SEEK_CUR) < 0)
    return fclose(file), perror("fseek"), 1;

  // print title in centre
  num_spaces = strlen("Rank") + score.width + 2; // 2 spaces separate fields
  for (i = 0; i < num_fields; i++)
    num_spaces += other_fields[i].width + 2;
  num_spaces = num_spaces / 2 - strlen(title) / 2;
  for (i = 0; i < num_spaces; i++)
    putchar(' ');
  puts(title);

  // print fields
  printf("Rank  %s", score.name);
  if (num_fields) { // no need to print extra spaces without extra fields
    num_spaces = score.width - strlen(score.name);
    for (i = 0; i < num_spaces; i++)
      putchar(' ');
  }
  for (i = 0; i < num_fields; i++) {
    printf("  %s", other_fields[i].name);
    if (i == num_fields - 1) // no need to print extra spaces
      break;
    num_spaces = other_fields[i].width - strlen(other_fields[i].name);
    for (j = 0; j < num_spaces; j++)
      putchar(' ');
  }
  putchar('\n');

  free(title), free(score.name);
  for (i = 0; i < num_fields; i++)
    free(other_fields[i].name);

  free(other_fields);
  fclose(file);
  return 0;
}

/*
 * fills in name and width of a field with the next 2 lines in a given file.
 * Returns 0 on success, -1 otherwise. errno set by arbstr or getnum indicates
 * what kind of failure.
 * (can't really be tested; just a very simple wrapper around already tested
 * functions, made as this sort of idiom is reused a lot)
 */
inline int
fill_field(FILE *file, Field *const field){
  Field tmpf;
  char *width_str;
  int name_len;

  if (!(tmpf.name = arbstr(16, file)))
    return -1; // errno already set to ENOMEM
  if (!(width_str = arbstr(16, file)))
    return -1;
  if (getnum(width_str, &tmpf.width) < 0)
    return free(tmpf.name), free(width_str), -1; // keeps errno by getnum
  free(width_str);
  name_len = strlen(tmpf.name);
  tmpf.width = name_len > tmpf.width ? name_len : tmpf.width;

  return field->name = tmpf.name, field->width = tmpf.width, 0;
}

inline void
usage(void){
  fprintf(stderr, "usage: lb [file]\n");
}
