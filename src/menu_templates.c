#include <ncurses.h>
#include <menu.h>
#include <form.h>
#include <string.h>

#include "misc.h"

/*
 * register_form() and login_form() are practically the same, except in the way
 * that they handle the provided name and passwd. Move that logic to this
 * function, and write the name and password to pointers. THE POINTERS MUST BE
 * FREED LATER ON AS THEY ARE malloc'd BY THIS FUNCTION.
 */
void
details_form(char const *const win_name, char **name, char **passwd){
  char *field_desc[] = {"name", "passwd"};
  FORM *form;
  // len_str += 2 for colon and space next to desc
  int lines, cols, len_str = strlen(field_desc[longest_str(field_desc, 2)])+2;
  WINDOW *win, *sub;
  FIELD *field_list[] = {new_field(1, COLS/4, 0, len_str, 0, 0),
                         new_field(1, COLS/4, 1, len_str, 0, 0), NULL};

  for (int i = 0; i < 2; i++)
    set_field_back(field_list[i], A_UNDERLINE),
      field_opts_off(field_list[i], O_STATIC);
  field_opts_off(field_list[1], O_PUBLIC);

  form = new_form(field_list);
  scale_form(form, &lines, &cols);
  set_form_win(form,
      (win = newwin(lines+2, cols+2, LINES/2-(lines+2)/2, COLS/2-(cols+2)/2))),
    keypad(win, TRUE);
  set_form_sub(form, (sub = derwin(win, lines, cols, 1, 1)));

  post_form(form);
  box(win, 0, 0);
  mvwprintw(win, 0, (cols+2)/2-strlen(win_name)/2, "%s", win_name);
  for (int i = 0; i < 2; i++)
    mvwprintw(sub, i, 0, "%-*s: ", len_str-2, field_desc[i]);
  mvprintw(0, 0,
      "NB: trailing whitespace in username or password will be trimmed off");
  wnoutrefresh(win), wnoutrefresh(stdscr);
  doupdate();

  curs_set(1);
  form_driver(form, REQ_VALIDATION); // puts cursor on form
  for (int ch; (ch = wgetch(win)) != KEY_F(1);)
    switch (ch) {
      case KEY_DOWN:
        form_driver(form, REQ_NEXT_FIELD);
        form_driver(form, REQ_END_LINE);
        break;
      case KEY_UP:
        form_driver(form, REQ_PREV_FIELD);
        form_driver(form, REQ_END_LINE);
        break;
      case '\t':
        form_driver(form, REQ_NEXT_FIELD);
        form_driver(form, REQ_END_LINE);
        break;
      case KEY_LEFT:
        form_driver(form, REQ_LEFT_CHAR);
        break;
      case KEY_RIGHT:
        form_driver(form, REQ_RIGHT_CHAR);
        break;
      case KEY_BACKSPACE:
        form_driver(form, REQ_DEL_PREV);
        break;
      case '\n':
        form_driver(form, REQ_VALIDATION); // refreshes field buffer
        if (current_field(form) == field_list[1]) {
          *name = str_no_tws(field_buffer(field_list[0], 0));
          *passwd = str_no_tws(field_buffer(field_list[1], 0));
          if (!*name && !*passwd)
            move(1, 0), clrtoeol(), printw("Enter a name and password -_-"),
              refresh();
          else if(!*passwd)
            move(1, 0), clrtoeol(), printw("Enter a password -_-"), refresh();
          else if(!*name)
            move(1, 0), clrtoeol(), printw("Enter a name -_-"), refresh();
          else {
            move(1, 0), clrtoeol(), refresh();
            goto loop_end;
          }
        }
        else
          form_driver(form, REQ_NEXT_FIELD);
        break;
      default:
        form_driver(form, ch);
        break;
    }
loop_end:
  curs_set(0);

  unpost_form(form);
  free_form(form);
  for (int i = 0; i < 2; i++)
    free_field(field_list[i]);
  delwin(win), delwin(sub);
}

/*
 * makes a selectable menu out of a list; returns the index selected. Returns
 * -1 if F1 is pressed.
 */
int
list_menu(char **list, int num_items){
  ITEM *item_list[num_items+1];
  int i, lines, cols, index = -1;
  MENU *menu;
  WINDOW *win, *sub;

  for (i = 0; i < num_items; i++)
    item_list[i] = new_item(list[i], NULL);
  item_list[num_items] = NULL;

  menu = new_menu(item_list);
  set_menu_mark(menu, NULL);
  scale_menu(menu, &lines, &cols);
  // draw a window in the middle of the screen, with some space to box it
  win = newwin(lines+2, cols+2, LINES/2-(lines+2)/2, COLS/2-(cols+2)/2),
      keypad(win, TRUE);
  sub = derwin(win, lines, cols, 1, 1);
  set_menu_win(menu, win), set_menu_sub(menu, sub);
  set_menu_fore(menu, COLOR_PAIR(1) | A_BLINK);

  box(win, 0, 0);
  post_menu(menu), wrefresh(win);
  
  for (int ch; (ch = wgetch(win)) != KEY_F(1);)
    switch (ch) {
      case KEY_UP:
        if (current_item(menu) == item_list[0])
          menu_driver(menu, REQ_LAST_ITEM);
        else
          menu_driver(menu, REQ_UP_ITEM);
        break;
      case KEY_DOWN:
        if (current_item(menu) == item_list[num_items-1])
          menu_driver(menu, REQ_FIRST_ITEM);
        else
          menu_driver(menu, REQ_DOWN_ITEM);
        break;
      case '\n':
        for (index = 0; current_item(menu) != item_list[index]; index++)
          ;
        goto list_menu_end;
    }

list_menu_end:
  unpost_menu(menu);
  for (i = 0; i < num_items; i++)
    free_item(item_list[i]);
  free_menu(menu);
  delwin(sub), delwin(win);
  return index;
}
