#ifndef LOGIN_H
#define LOGIN_H
#include <stdbool.h>

typedef void (*ActnPtr)(FILE *const);

char *login_form(FILE *const passwd_file);
void register_form(FILE *const passwd_file);
int check_details(FILE *const file, char const *const name,
    char const *const passwd);

#endif
