#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <limits.h>

#include "misc.h"

/*
 * returns index of the longest string in a string list; returns -1 if NULL,
 * skips NULL strings, but returns -1 if all strings are NULL.
 */
int
longest_str(char *const *const str_list, const size_t len){
  if (!str_list)
    return -1;
  int longest = 0;
  for (int i = 0; (unsigned)i < len; i++) {
    if (!str_list[i])
      continue;
    if (strlen(str_list[i]) >
        (!str_list[longest] ? 0 : strlen(str_list[longest])))
      longest = i;
  }

  return !str_list[longest] ? -1 : longest;
}

/*
 * returns a pointer to a version of the given str, excluding trailing
 * whitespace. Returns NULL if only whitespace (or strndup fails). String must
 * be freed when done with.
 */
char *
str_no_tws(char const *const str){
  char *newstr;
  int nul_pos, i;

  for (i = 0; str[i];) {
    for (; !isspace(str[i]) && str[i]; i++)
      ;
    nul_pos = i;
    for (; isspace(str[i]); i++)
      ;
  }

  if (!nul_pos)
    return NULL;
  newstr = strndup(str, nul_pos+1), newstr[nul_pos] = '\0';
  return newstr;
}

/*
 * get a line of arbitrary size and return a pointer to it. Returns NULL if
 * memory allocation functions are to fail, or if line contains only newline
 */
char *
arbstr(int len_step, FILE *const stream){
  int i, len = 0;
  char *p = NULL, ch, *alloc_tmp;

  for (i = 0; (ch = getc(stream)) != '\n'; i++){
    if (i >= len){
      len += len_step;
      // +1 in case i == len-1 rn, but next char is '\n'; i is still
      // incremented, meaning a potential buffer overflow when NUL terminated
      if (!(alloc_tmp = realloc(p, (len+1)*sizeof(char))))
        return free(p), NULL;
      p = alloc_tmp;
    }
    p[i] = ch;
    if (ch == EOF)
      return p;
  }
  if (!i)
    return NULL;
  p[i] = '\0';

  return p;
}

/*
 * returns number of executables in a path, writes a list of them to list. Last
 * element points to NULL (avoid needing to have size when freeing, just loop
 * until NULL). Why do I return a size then? You might want it for other
 * reasons (i.e. using list_menu).
 */
size_t
exec_list(char const *const path, char ***list){
  int i;
  char *full_path;
  DIR *dirp;
  struct dirent *cur_file;
  struct stat cur_stat;
  *list = NULL;

  if (!(dirp = opendir(path)))
    return 0;
  for (i = 0; (cur_file = readdir(dirp)); i++) {
    if (!strcmp(cur_file->d_name, ".") || !strcmp(cur_file->d_name, "..")) {
      i--;
      continue;
    }
    full_path = malloc((strlen(path)+strlen(cur_file->d_name)+2)*sizeof(char));
    sprintf(full_path, "%s/%s", path, cur_file->d_name);

    if (stat(full_path, &cur_stat) < 0)
      return free_exec_list(*list), free(full_path), 0;
    free(full_path);
    if (S_ISREG(cur_stat.st_mode) && !(cur_stat.st_mode & S_IXUSR)) {
      i--;
      continue;
    }

    *list = realloc(*list, (i+1)*sizeof(char*));
    (*list)[i] = strdup(cur_file->d_name);
  }
  closedir(dirp);
  *list = realloc(*list, (i+1)*sizeof(char*));
  (*list)[i] = NULL;

  return i;
}

inline void
free_exec_list(char **list){
  if (!list)
    return;
  for (int i = 0; list[i]; i++)
    free(list[i]);
  free(list);
}

/*
 * wrapper around strtol(); returns 0 on success, -1 on failure and sets errno.
 * EINVAL means a non-digit was encountered. num isn't changed unless the
 * function is successful.
 */
inline int
getnum(char const *const num_str, long *const num){
  long tmp_num;
  char *endptr;

  tmp_num = strtol(num_str, &endptr, 10);
  if (*endptr)
    return errno = EINVAL, -1;
  else if ((tmp_num == LONG_MIN || tmp_num == LONG_MAX) && errno == ERANGE)
    return -1;

  return *num = tmp_num, 0;
}
