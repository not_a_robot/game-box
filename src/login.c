/* logging in, registering, managing passwords, etc. */
#include <ncurses.h>
#include <menu.h>
#include <form.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "login.h"
#include "misc.h"
#include "menu_templates.h"
#include "sha-2/sha-256.h"

void hash_str(char const *const str, char *const result);

/*
 * returns the name supplied if both name and password are correct; NULL if
 * otherwise.
 */
char *
login_form(FILE *const passwd_file){
  int result;
  char *name = NULL, *passwd = NULL, passwd_hash[65];

  details_form("login", &name, &passwd);
  if (!name || !passwd)
    return NULL;

  hash_str(passwd, passwd_hash);

  rewind(passwd_file);
  result = check_details(passwd_file, name, passwd_hash);
  if (result == 1) {
    free(name), name = NULL;
    mvprintw(1, 0, "wrong password"), refresh(), getch();
  }
  else if (result < 0) {
    free(name), name = NULL;
    mvprintw(1, 0, "wrong username"), refresh(), getch();
  }
  free(passwd);

  return name;
}

void
register_form(FILE *const passwd_file){
  char *name = NULL, *passwd = NULL, passwd_hash[65];

  details_form("register", &name, &passwd);
  if (!name || !passwd)
    return;

  rewind(passwd_file);
  if (check_details(passwd_file, name, NULL) >= 0) {
    mvprintw(1, 0, "Name already exists!"), refresh(), getch();
    goto register_cleanup;
  }

  hash_str(passwd, passwd_hash);

  fseek(passwd_file, 0, SEEK_END);
  fwrite(name, strlen(name), 1, passwd_file);
  fwrite("\n", 1, 1, passwd_file);
  fwrite(passwd_hash, 64, 1, passwd_file);
  fwrite("\n", 1, 1, passwd_file);

register_cleanup:
  free(name), free(passwd);
}

/*
 * checks if given details are available in given file. A name and password
 * couple are separated by a newline (name\npasswd), and there's no need to
 * indicate the start/end of a name+passwd couple as they always come in pairs.
 * Returns 0 if details exist, 1 if only name exists, and -1 otherwise.
 */
int
check_details(FILE *const file, char const *const name,
    char const *const passwd){
  // name unlikely to be more than 31 chars, password should never exceed 64
  // chars when hashed: I malloc less this way.
  char *fname = arbstr(32, file), *fpasswd = arbstr(65, file);

  for (; *fname != EOF && *fpasswd != EOF;
      fname = arbstr(32, file), fpasswd = arbstr(65, file)) {
    if (!strcmp(name, fname)) {
      // sometimes only need to check name, check if passwd == NULL
      if (passwd && !strcmp(passwd, fpasswd))
        return free(fname), free(fpasswd), 0;
      else
        return free(fname), free(fpasswd), 1;
    }
    free(fname), free(fpasswd);
  }
  return free(fname), free(fpasswd), -1;
}

/*
 * takes a string (str) and writes its hash in hex string to result.
 */
void
hash_str(char const *const str, char result[65]){
  int i;
  uint8_t hash[32];

  calc_sha_256(hash, str, strlen(str));
  for (i = 0; i < 32; i++)
    result += sprintf(result, "%02x", hash[i]);
}
