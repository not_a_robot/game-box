#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/stat.h>
#include <errno.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>

#include "../login.h"

typedef struct {
  char *name, *passwd;
} Pair;

void test_check_details();
void test_check_details_p1(Pair ***pair_list, int *len);
void test_check_details_p2(Pair **pair_list, int len);
void test_check_details_p3(Pair **pair_list, int len);
void rand_str_file(int len, char *str, FILE *file);
void rand_str(int len, char *str);
bool check_exists(Pair *pair, Pair **list, int len);

int
main(void){
  srand((unsigned)time(NULL));
  printf("--- testing login.c functions ---\n\n");
  test_check_details(), printf("check_details passed\n");

  return printf("\nall tests passed\n"), 0;
}

void
test_check_details(){
  int len, i;
  Pair **pair_list;

  test_check_details_p1(&pair_list, &len);
  test_check_details_p2(pair_list, len);
  test_check_details_p3(pair_list, len);

  for (i = 0; i < len; i++) {
    free(pair_list[i]->name), free(pair_list[i]->passwd);
    free(pair_list[i]);
  }
  free(pair_list);
}

/*
 * make a list of name+passwd pairs and write them to a file. Check that
 * check_details finds them.
 */
void
test_check_details_p1(Pair ***pair_list, int *len){
  int name_len, passwd_len;
  FILE *file;
  *pair_list = NULL;
  *len = rand() % 127 + 1;

  if (mkdir("src/tests/data", S_IRWXU|S_IRGRP|S_IXGRP|S_IXOTH) &&
      errno != EEXIST)
    perror("can't create src/tests/data"), exit(1);

  if (!(file = fopen("src/tests/data/check_details", "w")))
    perror("can't open src/tests/data/check_details for writing"), exit(1);
  for (int i = 0; i < *len; i++) {
    *pair_list = realloc(*pair_list, (i+1)*sizeof(Pair*));
    (*pair_list)[i] = malloc(sizeof(Pair));

    name_len = rand() % 255 + 1;
    (*pair_list)[i]->name = malloc((name_len+1)*sizeof(char));
    rand_str_file(name_len, (*pair_list)[i]->name, file);

    passwd_len = rand() % 255 + 1;
    (*pair_list)[i]->passwd = malloc((passwd_len+1)*sizeof(char));
    rand_str_file(passwd_len, (*pair_list)[i]->passwd, file);
  }
  fclose(file);

  if (!(file = fopen("src/tests/data/check_details", "r")))
    perror("can't open src/tests/data/check_details for reading"), exit(1);
  for (int i = 0; i < *len; i++) {
    rewind(file);
    assert(!check_details(file, (*pair_list)[i]->name, (*pair_list)[i]->passwd));
  }
  fclose(file);
}

/*
 * test that it finds only names, but wrong password
 */
void
test_check_details_p2(Pair **pair_list, int len){
  int passwd_len, num_pairs = rand() % len + 1;
  FILE *file;
  char *passwd;

  if (!(file = fopen("src/tests/data/check_details", "r")))
    perror("can't open src/tests/data/check_details for reading"), exit(1);
  for (int i = 0; i < num_pairs; i++) {
    passwd_len = rand() % 255 + 1;
    passwd = malloc((passwd_len+1)*sizeof(char));
    rand_str(passwd_len, passwd);

    if (!strcmp(passwd, pair_list[i]->passwd)) {
      i--;
      free(passwd);
      continue;
    }

    /*
    if (check_exists(&buf, pair_list, len)) {
      i--;
      free(buf.name), free(buf.passwd);
      continue;
    }
    */

    assert(check_details(file, pair_list[i]->name, passwd) == 1);
    
    free(passwd);
  }
  fclose(file);
}

/*
 * test a bunch of non-existent pairs
 */
void
test_check_details_p3(Pair **pair_list, int len){
  int name_len, passwd_len, num_pairs = rand() % 127 + 1;
  FILE *file;
  Pair buf = {0};

  if (!(file = fopen("src/tests/data/check_details", "r")))
    perror("can't open src/tests/data/check_details for reading"), exit(1);
  for (int i = 0; i < num_pairs; i++) {
    name_len = rand() % 255 + 1;
    buf.name = malloc((name_len+1)*sizeof(char));
    rand_str(name_len, buf.name);

    passwd_len = rand() % 255 + 1;
    buf.passwd = malloc((passwd_len+1)*sizeof(char));
    rand_str(passwd_len, buf.passwd);

    if (check_exists(&buf, pair_list, len)) {
      i--;
      free(buf.name), free(buf.passwd);
      continue;
    }

    assert(check_details(file, buf.name, buf.passwd) < 0);
    
    free(buf.name), free(buf.passwd);
  }
  fclose(file);
}

void
rand_str_file(int len, char *str, FILE *file){
  char ch;

  for (int i = 0; i < len; i++) {
    while (!isalnum(ch = rand() % 126))
      ;
    str[i] = ch;
    putc(ch, file);
  }
  str[len] = '\0';
  putc('\n', file);
}

void
rand_str(int len, char *str){
  char ch;

  for (int i = 0; i < len; i++) {
    while (iscntrl(ch = rand() % 126))
      ;
    str[i] = ch;
  }
}

bool
check_exists(Pair *pair, Pair **list, int len){
  for (int i = 0; i < len; i++)
    if (!strcmp(list[i]->name, pair->name) &&
        !strcmp(list[i]->passwd, pair->passwd))
      return true;

  return false;
}
