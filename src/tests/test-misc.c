#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>
#include <ftw.h>
#include <limits.h>

#include "../misc.h"

void test_longest_str();
void test_longest_str_p1();
void test_longest_str_p2();
void test_longest_str_p3();
void test_str_no_tws();
void test_arbstr();
void test_exec_list();
void test_getnum();
char *rand_str(int len);
bool str_exists(char **master, int len, char *str);
int rm_file(const char *path, const struct stat *path_stat, int flag);

int
main(void){
  srand((unsigned)time(NULL));
  printf("--- testing misc.c functions ---\n\n");
  test_longest_str(), printf("longest_str passed\n");
  test_str_no_tws(), printf("str_no_tws passed\n");
  test_arbstr(), printf("arbstr passed\n");
  test_exec_list(), printf("exec_list passed\n");
  test_getnum(), printf("getnum passed\n");

  return printf("\nall tests passed\n"), 0;
}

void
test_longest_str(){
  for (int i = 0; i <= 16; i++)
    test_longest_str_p1();
  for (int i = 0; i <= 16; i++)
    test_longest_str_p2();
  test_longest_str_p3();
}

/*
 * phase 1: generate a random number of strings, each of a random length,
 * with random characters; keeps track of the longest index. If the index
 * kept track of is the same as the one returned by longest_str(), pass.
 */
void
test_longest_str_p1(){
  int num_strs = rand() % 64 + 2, len, longest = 0;
  size_t size;

  char *str_list[num_strs];
  for (int i = 0; i < num_strs; i++) {
    size = rand() % 126 + 2;
    len = size - 1; // for NUL terminator

    if (i && (unsigned)len > strlen(str_list[longest]))
      longest = i;
    str_list[i] = malloc(size * sizeof(char));
    char tmp;
    for (int j = 0; j < len; j++) {
      // remove escape sequences; they count as one char by strlen
      while (iscntrl(tmp = rand() % 126))
        ;
      str_list[i][j] = tmp;
    }
    str_list[i][len] = '\0';
  }
  assert(longest_str(str_list, num_strs) == longest);
  for (int i = 0; i < num_strs; i++)
    free(str_list[i]);
}

/*
 * phase 2: very simialr to phase 1, but randomly picked indexes are NULL;
 * checks if longest_str skips NULL strings.
 */
void
test_longest_str_p2(){
  int num_strs = rand() % 64 + 2, len;
  size_t size;
  struct { int len, index; } longest = {0, num_strs-1};

  char **str_list = calloc(num_strs, sizeof(char*));

  int num_null = rand() % (num_strs-1) + 1;

  for (int i = 0, index; i < num_strs-num_null; i++) {
    while (str_list[(index = rand() % (num_strs-1))])
      ;
    size = rand() % 126 + 2;
    len = size - 1; // for NUL terminator

    if ((unsigned)len >
        (!str_list[longest.index] ? 0 : strlen(str_list[longest.index])))
      longest.index = index, longest.len = len;
    else if ((unsigned)len == strlen(str_list[longest.index]) &&
        index < longest.index)
      longest.index = index, longest.len = len;
    str_list[index] = malloc(size * sizeof(char));

    char tmp;
    for (int j = 0; j < len; j++) {
      // remove escape sequences; they count as one char by strlen
      while (iscntrl(tmp = rand() % 126))
        ;
      str_list[index][j] = tmp;
    }
    str_list[index][len] = '\0';
  }

  assert(longest_str(str_list, num_strs) == longest.index);
  for (int i = 0; i < num_strs; i++)
    free(str_list[i]);
  free(str_list);
}

/*
 * phase 3: tests how longest_str handles all NULLs.
 */
void 
test_longest_str_p3(){
  assert(longest_str(NULL, rand()) < 0);

  int num_strs;
  while (!(num_strs = rand() % 128))
    ;
  char *str_list[num_strs];
  for (int i = 0; i < num_strs; i++)
    str_list[i] = NULL;
  assert(longest_str(str_list, num_strs) < 0);
}

void
test_str_no_tws(){
  char *msg, *str;

  msg = "msg with trailing ws   ";
  str = str_no_tws(msg);
  assert(!strcmp(str, "msg with trailing ws"));
  free(str);

  msg = "   msg with trailing ws and ws before   ";
  str = str_no_tws(msg);
  assert(!strcmp(str, "   msg with trailing ws and ws before"));
  free(str);

  msg = "msg_with_no_ws";
  str = str_no_tws(msg);
  assert(!strcmp(str, "msg_with_no_ws"));
  free(str);

  msg = "     \t\t  \n\n  \n     "; // just ws :)
  str = str_no_tws(msg);
  assert(!str);
  free(str);
}

void
test_arbstr(){
  int step, len, num_lines;
  FILE *stream;
  char ch, *str, **buf = NULL;

  if (mkdir("src/tests/data", S_IRWXU|S_IRGRP|S_IXGRP|S_IXOTH) &&
      errno != EEXIST)
    perror("can't create src/tests/data"), exit(1);

  step = rand() % 31 + 1;
  if (!(stream = fopen("src/tests/data/arbstr0", "w")))
    perror("can't open src/tests/data/arbstr0 for writing"), exit(1);
  fclose(stream);
  if (!(stream = fopen("src/tests/data/arbstr0", "r")))
    perror("can't open src/tests/data/arbstr0 for reading"), exit(1);
  assert(*(str = arbstr(step, stream)) == EOF);
  free(str);
  fclose(stream);

  step = rand() % 31 + 1;
  if (!(stream = fopen("src/tests/data/arbstr1", "w")))
    perror("can't open src/tests/data/arbstr1 for writing"), exit(1);
  putc('\n', stream);
  fclose(stream);
  if (!(stream = fopen("src/tests/data/arbstr1", "r")))
    perror("can't open src/tests/data/arbstr1 for reading"), exit(1);
  assert(!(str = arbstr(step, stream)));
  fclose(stream);

  // create a file with random lines and keep track of each line
  if (!(stream = fopen("src/tests/data/arbstr2", "w")))
    perror("can't open src/tests/data/arbstr2 for writing"), exit(1);
  num_lines = rand() % 127 + 1;
  for (int i = 0; i <= num_lines; i++) {
    len = rand() % 255 + 4;
    buf = realloc(buf, (i+1)*sizeof(char*));
    buf[i] = malloc((len+1)*sizeof(char));
    for (int j = 0; j < len; j++) {
      while (iscntrl(ch = rand() % 126))
        ;
      buf[i][j] = ch;
      putc(ch, stream);
    }
    buf[i][len] = '\0';
    putc('\n', stream);
  }
  fclose(stream);

  // check that lines gotten by arbstr are same as lines kept track of
  if (!(stream = fopen("src/tests/data/arbstr2", "r")))
    perror("can't open src/tests/data/arbstr2 for reading"), exit(1);
  for (int i = 0; i <= num_lines; i++) {
    step = rand() % 31 + 1;
    str = arbstr(step, stream);
    assert(!strcmp(str, buf[i]));
    free(str);
  }
  fclose(stream);

  for (int i = 0; i <= num_lines; i++)
    free(buf[i]);
  free(buf);
}

/*
 * creates a folder with randomly named files; some are gonna be executable;
 * keep track of them, run exec_list and check that the list is the same as
 * those kept track of.
 */
void
test_exec_list(){
  int i, j, num_files = rand() % 128 + 1, num_exec = rand() % num_files + 1;
  char *path = "src/tests/data/exec_list", *complete_list[num_files],
        *list_exec[num_exec], **list = NULL, *filename;
  FILE *file;
  for (i = 0; i < num_exec; i++)
    list_exec[i] = NULL;

  if (mkdir(path, S_IRWXU|S_IRGRP|S_IXGRP|S_IXOTH) && errno != EEXIST)
    printf("can't create %s: %s", path, strerror(errno)), exit(1);
  ftw(path, rm_file, FOPEN_MAX);
  chdir(path);

  // touch a bunch of files
  for (i = 0; i < num_files; i++, free(filename)) {
    filename = rand_str(rand()%32+1);
    if (!(file = fopen(filename, "w")))
      printf("can't open %s/%s for writing: %s",
          path, filename, strerror(errno)), exit(1);
    fclose(file);
    complete_list[i] = strdup(filename);
  }

  // make list of indexes of executable file names.
  for (int index, i = 0; i < num_exec; i++) {
    index = rand() % num_files;
    if (*list_exec && str_exists(list_exec, i, complete_list[index])) {
      i--;
      continue;
    }
    list_exec[i] = complete_list[index];
    chmod(list_exec[i], S_IXUSR|S_IXGRP|S_IXOTH);
  }

  // run exec_list; compare lists to each other
  assert(exec_list(".", &list) == (unsigned)num_exec);
  for (i = 0; i < num_exec; i++) {
    for (j = 0; j < num_exec && strcmp(list_exec[j], list[i]); j++)
      ;
    assert(j < num_exec);
    *list_exec[j] = '\0';
  }
  for (i = 0; i < num_files; i++)
    free(complete_list[i]);
  free_exec_list(list);
}

void
test_getnum(){
  int i, num_checks = rand() % 1024 + 1, len, j;
  long actual_num, num, old_num;
  char num_str[21]; // LONG_MIN occupies 20 chars

  // loop over random valid inputs
  for (i = 0; i < num_checks; i++) {
    actual_num = rand() % LONG_MAX + 1;
    sprintf(num_str, "%ld", actual_num);
    assert(!getnum(num_str, &num));
    assert(actual_num == num);
  }

  // check for overflow
  sprintf(num_str, "%lu", ULONG_MAX);
  old_num = num;
  assert(getnum(num_str, &num) < 0);
  assert(num == old_num);
  assert(errno == ERANGE);

  /*
   * can't check for underflow: long, long long and intmax_t all seem to have
   * the same length (at least on my machine), meaning I can't check for
   * LONG_MIN-1. __int128 is not portable afaik, so not using that.
   */

  // check for non-digits
  for (i = 0, num_checks = rand() % 1024 + 1; i < num_checks; i++) {
    actual_num = rand() % LONG_MAX + 1;
    len = rand() % 32 + 20;
    char bad_str[len+1];
    for (j = sprintf(bad_str, "%ld", actual_num) - 1; j < len; j++)
      bad_str[j] = rand() % 127 + 1;
    bad_str[j] = '\0';

    old_num = num;
    assert(getnum(bad_str, &num) < 0);
    assert(num == old_num);
    assert(errno == EINVAL);
  }
}

char *
rand_str(int len){
  char ch, *str = malloc((len+1)*sizeof(char));

  for (int i = 0; i < len; i++) {
    while (!isalnum(ch = rand() % 126))
      ;
    str[i] = ch;
  }
  str[len] = '\0';

  return str;
}

/*
 * checks that str exists in master list.
 */
bool
str_exists(char **master, int len, char *str){
  int i;
  for (i = 0; i < len && strcmp(master[i], str); i++)
    ;
  return i < len;
}

int
rm_file(const char *path, const struct stat *path_stat, int flag){
  (void)path_stat; // warns about unused variables otherwise
  return flag == FTW_F ? unlink(path) : flag == FTW_D ? 0 : -1;
}
