ifneq (, $(shell which clang))
	CC:=clang
else ifneq (, $(shell which gcc))
	CC:=gcc
else ifneq (, $(shell which cc))
	CC:=cc
else
	$(error "No C compiler found (!)")
endif
# defining POSIX used needed for some compilers
CFLAGS:=-Wall -Wextra -Wpedantic -Werror -std=c99 -O3 -D_POSIX_C_SOURCE=200809L
LDLIBS:=-lncurses -lmenu -lform
DIRS:=obj bin/tests
BINS:=bin/game-box bin/lb
TESTS:=$(patsubst src/tests/%.c,bin/tests/%,$(wildcard src/tests/*.c))

.PHONY: all dirs clean

all: dirs $(BINS) $(TESTS)

run: dirs $(BINS)
	./bin/game-box
tests: dirs $(TESTS)
	@for i in $(TESTS); do \
		$$i; \
	done

bin/game-box: obj/game-box.o obj/login.o obj/misc.o obj/menu_templates.o obj/sha-256.o
	$(CC) $(CFLAGS) $^ $(LDLIBS) -o $@
bin/lb: obj/lb.o obj/misc.o
	$(CC) $(CFLAGS) $^ -o $@
bin/tests/test-%: obj/%.o src/tests/test-%.c
	$(CC) $(CFLAGS) $^ -o $@
# no idea how to get the deps of another target... would be nice
bin/tests/test-login: obj/login.o obj/misc.o src/tests/test-login.c obj/menu_templates.o obj/sha-256.o 
	$(CC) $(CFLAGS) $^ $(LDLIBS) -o $@

obj/%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) -c $< -o $@
obj/%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@
obj/sha-256.o: src/sha-2/sha-256.c src/sha-2/sha-256.h
	$(CC) $(CFLAGS) -c $< -o $@

dirs: $(DIRS)
$(DIRS):
	@mkdir -p $(DIRS)

clean:
	rm -rf bin/* obj/*
