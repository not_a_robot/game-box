# Game Box

A project for school.

A "game box", with a login system, game menu, games (duh), and leaderboard.

## Features

- Completely standards compliant (C99 and POSIX).
- Guaranteed as much as reasonably possible to be safe/bug free.
  - Compiles with `-Wall -Wextra -Wpedantic -Werror` (note: only tested on
    `clang`; `clang` and `gcc` seem to have conflicting warnings for some
    things. `-Werror` can safely be turned off anyways, this is just a flex).
  - UBSan-ed, ASan-ed, and unit tests for automatically testable functions.

## Dependencies

- A C99 POSIX.1-2008 compiler.
- `ncurses` (available from POSIX).
- GNU make.
- https://github.com/amosnier/sha-2 (for hashing, to store passwords securely
  will be included as a submodule).
